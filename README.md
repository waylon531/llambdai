# LLAMBDAi

LLAMBDAi is an interpreter for LLAMBDA, a purely function lambda-calculus-based
language. LLAMBDAi is an abstraction over a simply typed lambda calculus, and
has useful datatypes like lisp-style lists and some types for doing basic IO.


## Documentation

There's a manual! It's currently hosted [here](http://web.cecs.pdx.edu/~cudew/LLambai_Docs.pdf).
The manual documents both built-in language features, as well as the standard
library included with LLAMBDAi.

## Usage

Right now LLAMBDAi only accepts programs on stdin as it is meant for running
short snippets of code via irc. To run a program from a file you can pipe it
to LLAMBDAi, with `cat file.llm | llambdai`

## Examples

Multiplying the first and second elements of a list
```
let list = cons 2 (cons 4 nil); 
let first = car list; 
let second = car ( cdr list ); 
( * first second )
```

Recursively doubling a number, `double x count` returns `x*2^count`
```
let x = 1; 
let count = 3; 
let double = \x count .
    ife count (double (* x 2) (- count 1) ) (x) ;
double x count
```


LLAMBDA includes facilities for network communication
```
let io = connect "localhost:80";
let result = write io "GET /";
let result = write io 10;
nil
```

