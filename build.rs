extern crate lalrpop;

fn main() {
    lalrpop::Configuration::new()
        //.force_build(true)
        .use_cargo_dir_conventions()
        .process()
        .unwrap();
}
