//    A toy compiler for a lambda-calculus language
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::fmt;
use std::io::{self,Read,Write};
use std::sync::{Arc,Mutex};
use std::collections::HashMap;

pub trait IO: io::Read + io::Write {}
impl IO for ::std::net::TcpStream {}
#[derive(Clone)]
pub struct IOBox {
    io: Arc<Mutex<Box<IO>>>    
}
impl IOBox {
    pub fn new(boxed_io: Box<IO>) -> IOBox {
        IOBox {
            io: Arc::new(Mutex::new(boxed_io))
        }
    }
}
impl PartialEq for IOBox {
    fn eq(&self, _other: &IOBox) -> bool {
        //IO objects are never equal to each other
        false
    }
}
impl fmt::Debug for IOBox {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "IO Container")
    }
}
impl Read for IOBox {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.io.lock().unwrap().read(buf)
    }
}
impl Write for IOBox {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.io.lock().unwrap().write(buf)
    }
    fn flush(&mut self) -> io::Result<()> {
        self.io.lock().unwrap().flush()
    }
    
}
//Things that give you a result
#[derive(PartialEq,Debug,Clone)]
pub enum Expr {
    Number(f64),
    String(String),
    Nil,
    Table(HashMap<String,Box<Expr>>),
    // Table to index, index
    Index(Box<Expr>,Box<Expr>),
    Func(Func),
    Var(String), //Variable name
    //Assign(String,Box<Expr>), //var name, thing to assign
    Apply(Box<Expr>,Vec<Box<Expr>>), //function, arguments
    Loop(Box<Expr>,Vec<(String,Box<Expr>)>,Box<Expr>), //Number of loops, vec of lets, next expression
    List(Box<Expr>,Box<Expr>),
    //Lib to load, next expression
    Use(Box<Expr>,Box<Expr>),
    IO(IOBox),
}
impl fmt::Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Expr::Number(ref n) => write!(f, "{}", n),
            &Expr::String(ref n) => write!(f, "{}", n),
            e @ _ => write!(f, "{:?}", e)
        }
    }
}
#[derive(PartialEq,Debug,Clone)]
pub enum Func {
    Loop, //Not actually a function
    Use, //Not actually a function
    Add,
    Sub,
    Div,
    Mul,
    Greater,
    Less,
    Mod,
    Floor,
    Ceil,
    Round,
	Rand,
    Cons,
    Car,
    Cdr,
    //If,
    IfElse,
    IfLT,
    //Abs,
    Read,
    Write,
    Connect,
    Lambda(Vec<String>,Box<Expr>),
    //Named(String)
}
impl Func {
    pub fn num_args(&self) -> usize {
        use self::Func::*;
        match *self {
            Add | Sub | Div | Mul 
                | Greater | Less | Mod 
                | Cons | Write => 2,
            
            Floor | Ceil | Round 
            | Car | Cdr 
            | Connect | Read=> 1,
            IfElse => 3,
            IfLT => 4,
            Lambda(ref args, _) => args.len(),
            Loop | Use | Rand => 0,
        }
    }
}

impl fmt::Display for Func {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            e @ _ => write!(f, "{:?}", e)
        }
    }
}
