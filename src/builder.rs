//    A toy compiler for a lambda-calculus language
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::borrow::Cow;
use std::collections::HashMap;
use std::io::{Read,Write};
use std::net::TcpStream;


use stacker;

use ast::{Expr,Func,IOBox};

use error::{Error,Result};

use libs::load_libs;

use rand;

//#[derive(Clone)]
//pub enum Type {
//    //Func pointer, num of arguments
//    Func,
//    //Value pointer
//    Num,
//    String
//}
#[derive(Debug)]
pub struct Builder<'cow> {
    frames: Vec<HashMap<String,Cow<'cow, Expr>>>,
    libs: HashMap<String,Expr>, //Libs are loaded into here
    lib: bool,
}
impl<'cow> Builder<'cow> {
    pub fn new() -> Builder<'cow> {
        let mut frames = Vec::new();
        frames.push(HashMap::new());
        Builder {
            frames,
            lib: false,
            libs: load_libs()
        }
    }
    fn get_lib(&self,s: &String) -> Result<&Expr> {
        self.libs.get(s).ok_or(Error::LibNotFound(s.clone()))
    }
    fn get_frame(&mut self) -> &mut HashMap<String,Cow<'cow,Expr>> {
        let len = self.frames.len()-1;
        &mut self.frames[len]
    }
    fn enter(&mut self) {
        //Don't change stack frames when loading a lib
        if !self.lib {
            let cloned_map = self.get_frame().clone();
            self.frames.push(cloned_map);
        }
    }
    fn leave(&mut self) {
        //Don't change stack frames when loading a lib
        if !self.lib {
            self.frames.pop();
        }
    }
    fn get_var(&mut self, v: &String)-> Result<Cow<Expr>> {
        self.get_frame().get(v).cloned().ok_or(Error::VariableNotFound(v.clone()))
    }
    fn add_var(&mut self, v: &String, ex: Expr) {
        self.get_frame().insert(v.clone(),Cow::Owned(ex));
    }
    pub fn run<'a>(&mut self, ast: &'a Expr) -> Result<Expr> {

        self.traverse(&ast)
    }
    fn traverse<'a>(&mut self, expr: &'a Expr) -> Result<Expr> {
        //Errors only happen once so cloning them is fine
        #[cfg(feature = "debug")]
        {
            println!("");
            println!("----BEGIN----");
            println!("");
            println!("EXPR: {:?}",expr);
            println!("");
            println!("BUILDER: {:?}",self);
            println!("");
            println!("-----END-----");
            println!("");
        }
        stacker::maybe_grow(32 * 1024 * 1024, 1024 * 1024 * 1024, || {
        match expr {
            &Expr::Use(ref lib, ref next) => {
                let processed_lib = self.traverse(lib)?;
                match processed_lib {
                    Expr::String(ref s) => {
                        self.lib = true;
                        //Potential fix: make libs Arc<>
                        //libs are borrowed to get traversed
                        //not too important use only happens a couple times per program
                        let lib = self.get_lib(s)?.clone();
                        self.traverse(&lib)?;
                        self.lib = false;
                        self.traverse(next)
                    },
                    _ => Err(Error::TypeError(Func::Use,vec![*lib.clone()]))
                }
            },
            &Expr::Apply(ref ex,ref args) => {
                match self.traverse(ex)? {
                    Expr::Func(f) => {
                        macro_rules! apply {
                            ($($func:pat => $result:expr),*$(,)*) => {{
                                match f {
                                    $(
                                        $func => if args.len() != f.num_args() {
                                            Err(Error::WrongNumberOfArgs(f.clone(),args.len()))
                                        } else {
                                            $result
                                        } 
                                     ),*,
                                    _ => unimplemented!()
                                }
                            }}

                        }
                        macro_rules! types_2 {
                            ($($pat:pat => $expr:expr),*$(,)*) => {{
                                match (self.traverse(&*args[0])?,self.traverse(&*args[1])?) {
                                    $(
                                        $pat => $expr
                                    ),*,
                                    (a,b) => return Err(Error::TypeError(f.clone(),vec![a,b]))
                                }
                            }}
                        }
                        macro_rules! types_1 {
                            ($($pat:pat => $expr:expr),*$(,)*) => {{
                                match self.traverse(&*args[0])? {
                                    $(
                                        $pat => $expr
                                    ),*,
                                    a => return Err(Error::TypeError(f.clone(),vec![a]))
                                }
                            }}
                        }
                        macro_rules! types_2_no_default {
                            ($($pat:pat => $expr:expr),*$(,)*) => {{
                                match (self.traverse(&*args[0])?,self.traverse(&*args[1])?) {
                                    $(
                                        $pat => $expr
                                    ),*,
                                }
                            }}
                        }
                        apply!(
                            Func::Add => types_2! {
                                (Expr::Number(a),Expr::Number(b)) => Ok(Expr::Number(a+b)),
                                (Expr::String(a),Expr::String(b)) => Ok(Expr::String(a+&b)),
                                (Expr::String(a),Expr::Number(b)) => Ok(Expr::String(format!("{}{}",a,b))),
                                (Expr::Number(a),Expr::String(b)) => Ok(Expr::String(format!("{}{}",a,b)))
                            },
                            Func::Sub => types_2!{
                                (Expr::Number(a),Expr::Number(b)) => Ok(Expr::Number(a-b))
                            },
                            Func::Mul => types_2!{
                                (Expr::Number(a),Expr::Number(b)) => Ok(Expr::Number(a*b)),
                            },
                            Func::Div => types_2!{
                                (Expr::Number(a),Expr::Number(b)) => Ok(Expr::Number(a/b)),
                            },
                            Func::Mod => types_2!{
                                (Expr::Number(a),Expr::Number(b)) => Ok(Expr::Number(a%b)),
                            },
                            Func::Greater => types_2!{

                                (Expr::Number(a),Expr::Number(b)) => {
                                    if a > b {
                                        Ok(Expr::Number(1.0))
                                    } else {
                                        Ok(Expr::Number(0.0))
                                    }
                                },
                            },
                            Func::Less => types_2!{
                                (Expr::Number(a),Expr::Number(b)) => {
                                    if a < b {
                                        Ok(Expr::Number(1.0))
                                    } else {
                                        Ok(Expr::Number(0.0))
                                    }
                                },
                            },
                            Func::Rand => return Ok(Expr::Number(rand::random())),
                            Func::Cons => types_2_no_default! {
                                (a,b) => return Ok(Expr::List(Box::new(a),Box::new(b)))//TODO: do I want to traverse?
                            },
                            Func::Floor => types_1!{
                                Expr::Number(num) => Ok(Expr::Number(num.floor())),
                            },
                            Func::Ceil => types_1!{
                                Expr::Number(num) => Ok(Expr::Number(num.ceil())),
                            },
                            Func::Round => types_1!{
                                Expr::Number(num) => Ok(Expr::Number(num.round())),
                            },
                            Func::IfElse => match (self.traverse(&*args[0])?,&*args[1],&*args[2]) 
                            {
                                (Expr::Number(cond), ref ex, ref else_ex) => {
                                    if cond == 0.0 {
                                        self.traverse(else_ex)
                                    } else {
                                        self.traverse(ex)
                                    }
                                },
                                (Expr::Nil, ref _ex, ref else_ex) => {
                                    //Nil is always false,
                                    //always branch to else
                                    self.traverse(else_ex)
                                },
                                (_, ref ex, _,) => self.traverse(ex)
                            },
                            Func::IfLT => match (self.traverse(&*args[0])?,&*args[1],&*args[2],&*args[3]) 
                            {
                                (Expr::Number(cond), ref g_ex, ref eq_ex, ref l_ex) => {
                                    if cond == 0.0 {
                                        self.traverse(eq_ex)
                                    } else if cond > 0.0 {
                                        self.traverse(g_ex)
                                    } else {
                                        self.traverse(l_ex)
                                    }
                                },
                                (Expr::Nil, _, ref else_ex, _) => {
                                    //Nil is always false,
                                    //always branch to else
                                    self.traverse(else_ex)
                                },
                                (_, ref ex, _, _) => self.traverse(ex)
                            },
                            Func::Car => types_1!{
                                Expr::List(ref a,_) => self.traverse(a),
                            },
                            Func::Cdr => types_1!{
                                Expr::List(_,ref b) => self.traverse(b),
                            },
                            Func::Read => types_1!{
                                Expr::IO(io_box) => {
                                    let mut buf = [0u8];
                                    //Clone is annoying but not exponentiall bad
                                    io_box.clone().read(&mut buf)?;
                                    let num: u8 = buf[0];
                                    Ok(Expr::Number(num as f64))
                                }
                            },
                            Func::Write => types_2!{
                                (Expr::IO(io_box),Expr::Number(num)) => {
                                    io_box.clone().write_all(&[(num % 256.0) as u8])?;
                                    Ok(Expr::Nil)
                                },
                                (Expr::IO(io_box),Expr::String(string)) => {
                                    io_box.clone().write_all(string.as_bytes())?;
                                    Ok(Expr::Nil)
                                },
                            },
                            Func::Connect => types_1!{
                                Expr::String(ip) => {
                                    Ok(Expr::IO(IOBox::new(Box::new(TcpStream::connect(&ip)?))))
                                }
                            },
                            Func::Lambda(ref vars,ref expr) => {
                                let mut rev = args.iter();
                                //rev.reverse();
                                self.enter();
                                for var in vars {
                                    //Huh, I need some sort of shallow traverse
                                    //so variables aren't self-referential
                                    //
                                    //Or I need to rename stuff for each recursion
                                    //or have contexts
                                    //
                                    //Var stack: each var with the same name gets
                                    //pushed onto the var stack
                                    //  When you're reading the top thing on the stack
                                    //  gets popped off so it's not self referential
                                    let expr = match rev.next() {
                                        Some(e) => e,
                                        None => return Err(Error::WrongNumberOfArgs(f.clone(),args.len()))
                                    };
                                    let evaluated_arg = self.traverse(expr)?;
                                    //let evaluated_arg =*rev.pop().ok_or(Error::NotEnoughArgs(f_clone.clone()))?;

                                    self.add_var(var,evaluated_arg);
                                }
                                let res = self.traverse(expr);
                                self.leave();
                                res

                            }
                        )
                    },
                    _ => Err(Error::InvalidApply(expr.clone()))
                }
            },
            &Expr::Loop(ref num,ref lets,ref next) => {
                let final_num = match self.traverse(num)? {
                    Expr::Number(num) => num,
                    e @ _ => return Err(Error::TypeError(Func::Loop,vec![e]))
                };
                for _ in 0 .. final_num as u64 {
                    for &(ref var,ref expr) in lets {
                        let value = self.traverse(expr)?;
                        self.add_var(var,value);
                    }
                }
                self.traverse(next)

            },
            //Variables should only contain funcs strings or nums
            //should they really though?
            &Expr::Var(ref v) => {
                //let value = self.values.get(v).cloned().ok_or(Error::VariableNotFound(v.clone()))?;
                Ok(self.get_var(v)?.into_owned())
                    //LOL I HAVE NO IDEA WHAT IM DOING
                    //self.traverse(&value)
                    //Ok(value)
            }, 
            &Expr::Index(ref table, ref index) => {
                match self.traverse(index)? {
                    Expr::String(i) => {
                        match self.traverse(table)? {
                            Expr::Table(t) => {
                                //TODO: should I traverse this
                                //TODO: should this be a clone? probably
                                //Ok(t.get(&i)?.clone())
                                match t.get(&i) {
                                    Some(val) => Ok(*(val.clone())),
                                    None => Err(Error::NoSuchIndex(i.clone()))
                                }
                            },
                            _e => Err(Error::NotATable(_e.clone()))
                        }
                    }
                    _e =>  Err(Error::NotAnIndex(_e.clone()))
                }
            },
            //Handle funcs with 0 args
            &Expr::Func(Func::Rand) => return Ok(Expr::Number(rand::random())),
            &Expr::Func(Func::Lambda(ref args,ref next)) if args.len() == 0 => {
                self.traverse(next)
            },
            //If it's a number or string it's okay to just return it
            ex @ _ => Ok(ex.clone()) //It's cheap to clone these
        }
        })
    }
}
