//    A toy compiler for a lambda-calculus language
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub fn remove_comments<T: AsRef<str>>(s: T) -> String {
    let mut res = String::new();
    let mut mode = Mode::None;
    let mut last = ' ';
    for c in s.as_ref().chars() {
        macro_rules! push { () => {
            res.push(c);
        }}
        match (mode,c) {
            (Mode::None, '"') => {
                mode = Mode::String;
                push!();
            },
            (Mode::String, '"') => {
                mode = Mode::None;
                push!();
            },
            (Mode::None, '\'') => {
                mode = Mode::AltString;
                push!();
            },
            (Mode::AltString, '\'') => {
                mode = Mode::None;
                push!();
            },
            (Mode::None, '#') => {
                if last == '(' {
                    let _ = res.pop(); //Get rid of the preceding (
                    mode = Mode::Comment(1);
                } else {
                    mode = Mode::LineComment;
                }
            },
            (Mode::Comment(i), '#') => {
                if last == '(' {
                    mode = Mode::Comment(i+1);
                }
            },
            (Mode::Comment(i), ')') => {
                if last == '#' {
                    if i == 1 {
                        mode = Mode::None;
                    } else {
                        mode = Mode::Comment(i-1);
                    }
                }
            },
            (Mode::LineComment, '\n') => {
                mode = Mode::None;
                push!();
            },
            (Mode::LineComment, _) => {},
            (Mode::Comment(_), _) => {},
            (_,_) => {
                push!();
            }
        }
        last = c;
    } 
    return res;
}
#[derive(Clone,Copy)]
enum Mode {
    None,
    String,
    AltString,
    Comment(usize), //Comment depth
    LineComment
}
