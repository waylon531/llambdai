//    A toy compiler for a lambda-calculus language
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::io;
use std::result;
use std::fmt;

use ast::{Expr,Func};

use lalrpop_util::ParseError;
use std::fmt::{Display,Debug};

pub trait DisplayBug: Display + Debug {}
pub type Result<T> = result::Result<T,Error>;

impl<L,T,E> DisplayBug for ParseError<L,T,E> 
where L: fmt::Debug + fmt::Display,
      T: fmt::Debug + fmt::Display,
      E: fmt::Debug + fmt::Display
{}

#[derive(Debug)]
pub enum Error {
    TypeError(Func,Vec<Expr>),
    WrongNumberOfArgs(Func,usize),
    VariableNotFound(String),
    InvalidApply(Expr),
    LibNotFound(String),
    IOError(io::Error),
    ParseError(String),
    NotATable(Expr),
    NotAnIndex(Expr),
    NoSuchIndex(String),
}
impl PartialEq for Error {
    fn eq(&self, other: &Error) -> bool {
        match (self,other) {
            //TODO: implement this
            //Probably through an errorbox which always returns false
            (_,_) => false
        }
    }
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::IOError(error)
    }
}

impl<L,T,E> From<ParseError<L,T,E>> for Error 
where L: fmt::Debug + fmt::Display,
      T: fmt::Debug + fmt::Display,
      E: fmt::Debug + fmt::Display
{
    fn from(error: ParseError<L,T,E>) -> Self {
        Error::ParseError(format!("{}",error).to_owned())
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::TypeError(ref func,ref e) => write!(f, "Type error: {:?} passed to {}",e,func),
            Error::WrongNumberOfArgs(ref func,ref num) => write!(f, "Wrong number of arguments passed to {}; {} passed, {} expected",func,num,func.num_args()),
            Error::VariableNotFound(ref v) => write!(f,"Variable not found: {}",v),
            Error::InvalidApply(ref e) => write!(f,"Invalid apply: {}\nYou tried to use something that wasn't a function.",e),
            Error::LibNotFound(ref l) => write!(f,"Library not found: {}",l),
            Error::IOError(ref err) => write!(f,"IO error: {:?}",err),
            Error::ParseError(ref err) => write!(f,"Parse error: {}",err),
            Error::NotATable(ref err) => write!(f,"Not a table: {}",err),
            Error::NotAnIndex(ref err) => write!(f,"Indicies need to be strings: {}",err),
            Error::NoSuchIndex(ref err) => write!(f,"No such index: {}",err),
        }
    }
}

