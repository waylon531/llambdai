//    A toy compiler for a lambda-calculus language
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
use std::collections::HashMap;
use ast::{Expr};
use llambda;
use comment;
pub fn load_libs() -> HashMap<String,Expr>{
    let mut map: HashMap<String,Expr> = HashMap::new();
    let libs = vec![
        ("std::list",include_str!("std/list.llm")),
        ("std::core",include_str!("std/core.llm")),
        ("std::string",include_str!("std/string.llm"))
    ];
    for (name,lib) in libs {
        let input = comment::remove_comments(&lib);
        //This unwmrap should never be triggered, if the stdlib
        //parses (which it always should)
        let ast = llambda::parse_Expr(&input).unwrap();
        map.insert(name.to_owned(),*ast);
    }
    map
}
