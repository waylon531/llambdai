//    A toy compiler for a lambda-calculus language
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate rand;
extern crate stacker;
extern crate lalrpop_util;
mod ast;
mod builder;
mod comment;
mod error;
mod libs;
mod llambda {
    include!(concat!(env!("OUT_DIR"), "/llambda.rs"));
}
#[cfg(test)]
mod test;
use std::io;
use std::io::BufRead;
use ast::{Expr};
use builder::Builder;
fn main() {
    let mut input = String::new();
    {
        let stdin = io::stdin();
        let mut handle = stdin.lock();
        handle.read_line(&mut input).expect("Bad input?");
    }
    let result = get_result(input.clone());
    match result {
        Ok(result) => println!("{}", result),
        Err(e) =>println!("{}",e)
    }
}
pub fn get_result(input: String) -> error::Result<Expr> {
    #[cfg(feature = "debug")]
    println!("Removing comments");
    let input = comment::remove_comments(&input);
    #[cfg(feature = "debug")]
    println!("Building AST");
    let ast = llambda::parse_Expr(&input)?;
    #[cfg(feature = "debug")]
    println!("AST: {:?}",ast);
    #[cfg(feature = "debug")]
    println!("Built AST");

    let mut builder = Builder::new();

    builder.run(&ast)

}
