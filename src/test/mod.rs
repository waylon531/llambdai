mod stdlib;
    mod comment {
        use comment::remove_comments;
        #[test]
        fn basic_comment_test() {
            assert_eq!(
                remove_comments("test(#comment#) string"),
                "test string".to_owned());
        }
        #[test]
        fn basic_line_comment_test() {
            assert_eq!(
                remove_comments("test#comment#) string"),
                "test".to_owned());
        }
        #[test]
        fn basic_multiline_line_comment_test() {
            assert_eq!(
                remove_comments("test#comment\nstring"),
                "test\nstring".to_owned());
        }
        #[test]
        fn another_comment_test() {
            assert_eq!(
                remove_comments(r#"test(# comment #) string "no(#comment#)" "#),
                r#"test string "no(#comment#)" "#.to_owned());
        }
        #[test]
        fn multiline_comment_test() {
            assert_eq!(
                remove_comments("test(#comment#);\nstring"),
                "test;\nstring".to_owned());
        }
        #[test]
        fn nested_comment_test() {
            assert_eq!("8",remove_comments("(# test (# comment #) #)8".to_owned()));
        }
        #[test]
        fn map_comment_test() {
            assert_eq!(
                remove_comments("use \"std::list\";(# import the list library #)\nlet l = (list4 2 1 7 4);(#create a list #)\nlet mapped = (map (\\x. (* 2 x)) l);\n( fold  + mapped )(# Sum the list #)"),
                "use \"std::list\";\nlet l = (list4 2 1 7 4);\nlet mapped = (map (\\x. (* 2 x)) l);\n( fold  + mapped )");
        }
    }
    mod ast { 
        use ast::{Func,Expr};
        use llambda;
        #[test]
        fn basic_test() {
            assert_eq!(llambda::parse_Expr("( + 2 1 )").unwrap(),Box::new(Expr::Apply(Box::new(Expr::Func(Func::Add)),vec![Box::new(Expr::Number(2f64)), Box::new(Expr::Number(1f64))])));
        }
        #[test]
        fn less_basic_test() {
            assert_eq!(llambda::parse_Expr("( * ( + 2 1 ) 3 )").unwrap(),Box::new(Expr::Apply(Box::new(Expr::Func(Func::Mul)),vec![Box::new( Expr::Apply(Box::new(Expr::Func(Func::Add)),vec![Box::new(Expr::Number(2f64)), Box::new(Expr::Number(1f64))])),Box::new(Expr::Number(3f64))])));
        }
        #[test]
        fn lambda_test() {
            assert_eq!(llambda::parse_Expr(r"( \x.(+ x 1) 1 )").unwrap(),Box::new(Expr::Apply(Box::new(Expr::Func(Func::Lambda(vec!["x".to_owned()],Box::new(Expr::Apply(Box::new(Expr::Func(Func::Add)),vec![Box::new(Expr::Var("x".to_owned())),Box::new(Expr::Number(1f64))]))))),vec![Box::new(Expr::Number(1f64))])));
        }
        #[test]
        fn let_test() {
            assert_eq!(llambda::parse_Expr(r"let x = 1; x").unwrap(),Box::new(Expr::Apply(Box::new(Expr::Func(Func::Lambda(vec!["x".to_owned()],Box::new(Expr::Var("x".to_owned()))))),vec![Box::new(Expr::Number(1f64))])));
        }
        #[test]
        fn let_func_test() {
            assert_eq!(llambda::parse_Expr(r"let x = *; x").unwrap(),Box::new(Expr::Apply(Box::new( Expr::Func(Func::Lambda(vec!["x".to_owned()],Box::new(Expr::Var("x".to_owned()))))),vec![Box::new(Expr::Func(Func::Mul))])));
        }
    }
    mod core {
        use get_result;
        use ast::Expr;
        #[test]
        fn simple_number_test() {
            assert_eq!(Ok(Expr::Number(8f64)),get_result("8".to_owned()));
        }
        #[test]
        fn infix_add_test() {
            assert_eq!(Ok(Expr::Number(8f64)),get_result("2 `+` 6".to_owned()));
        }

        #[test]
        fn basic_test_number() {
            assert_eq!(Ok(Expr::Number(3.0)),get_result("( + 2 1 )".to_owned()));
        }
        #[test]
        fn basic_test_string() {
            assert_eq!(Ok(Expr::String("Hello world!".to_owned())),get_result(r#"( + "Hello " "world!" )"#.to_owned()));
        }
        #[test]
        fn alternate_test_string() {
            assert_eq!(Ok(Expr::String("Hello world!".to_owned())),get_result(r#"( + 'Hello ' 'world!' )"#.to_owned()));
        }
        #[test]
        fn alternate_test_string_quotes() {
            assert_eq!(Ok(Expr::String("Hello \"world!\"".to_owned())),get_result(r#"( + 'Hello ' '"world!"' )"#.to_owned()));
        }
        #[test]
        fn string_quotes() {
            assert_eq!(Ok(Expr::String("\"".to_owned())),get_result(r#"'"'"#.to_owned()));
        }
        #[test]
        fn loop_test() {
            assert_eq!(Ok(Expr::Number(8f64)),get_result("let x = 1; loop 3 { let x = ( * x 2 ) } x".to_owned()));
        }
        #[test]
        fn katakana_test() {
            assert_eq!(Ok(Expr::Number(1f64)),get_result("let ツ = 1; ツ".to_owned()));
        }
        #[test]
        fn two_thing_test() {
            assert_eq!(Ok(Expr::Number(4f64)),get_result(r"let two_thing = \x.(x 2 2); (two_thing * )".to_owned()));
        }
        #[test]
        fn forty_two_test() {
            assert_eq!(Ok(Expr::Number(42f64)),get_result(r"let seven = (+ 4 3); let square = \x.(* x x); ( - (square seven) seven )".to_owned()));
        }
        #[test]
        fn expand_loop_test() {
            assert_eq!(Ok(Expr::Number(1024f64*1024f64)),get_result("let x = 1; loop 20 { let x = ( + x x ) } x".to_owned()));
        }
        #[test]
        fn big_loop_test() {
            assert_eq!(Ok(Expr::Number(100f64)),get_result("let x = 0; loop 100 { let x = ( + x 1 ) } x".to_owned()));
        }
        #[test]
        fn multi_loop_test() {
            assert_eq!(Ok(Expr::Number(30f64)),get_result("let x = 0; loop 4 { let x = ( + x 1 ) let x = ( * x 2 ) } x".to_owned()));
        }
        #[test]
        fn lambda_add_test() {
            assert_eq!(Ok(Expr::String("testthing".to_owned())),get_result(r#"let add = \x y. ( + x y ); (add "test" "thing")"#.to_owned()))
        }
        #[test]
        fn car_test() {
            assert_eq!(Ok(Expr::Number(4.0)),get_result(r#"( car ( cons 4 2 ) )"#.to_owned()))
        }
        #[test]
        fn cdr_test() {
            assert_eq!(Ok(Expr::Number(2.0)),get_result(r#"( cdr ( cons 4 2 ) )"#.to_owned()))
        }
        #[test]
        fn double_cdr_test() {
            assert_eq!(Ok(Expr::Number(2.0)),get_result(r#"( cdr ( cdr ( cons 4 ( cons 4 2 ) ) ) )"#.to_owned()))
        }
        #[test]
        fn car_cdr_test() {
            assert_eq!(Ok(Expr::Number(4.0)),get_result(r#"( car ( cdr ( cons 8 ( cons 4 2 ) ) ) )"#.to_owned()))
        }
        #[test]
        fn iflt_test() {
            assert_eq!(
                Expr::Number(2.0),
                get_result(r#"( iflt 0 1 2 3)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn iflt_less_test() {
            assert_eq!(
                Expr::Number(3.0),
                get_result(r#"( iflt -10 1 2 3)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn iflt_greater_test() {
            assert_eq!(
                Expr::Number(1.0),
                get_result(r#"( iflt 10 1 2 3)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn if_test() {
            assert_eq!(
                Expr::Number(2.0),
                get_result(r#"( ife 0 1 2)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn if_no_parens_test() {
            assert_eq!(
                Expr::Number(2.0),
                get_result(r#"ife 0 1 2"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn if_infix_test() {
            assert_eq!(
                Expr::Number(2.0),
                get_result(r#"0 `ife` 1 2"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn else_test() {
            assert_eq!(
                Expr::Number(1.0),
                get_result(r#"( ife 1 1 2)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn greater_test() {
            assert_eq!(
                Expr::Number(1.0),
                get_result(r#"( ife ( > 2 1) 1 2)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn less_test() {
            assert_eq!(
                Expr::Number(2.0),
                get_result(r#"( ife ( < 2 1) 1 2)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn loop_expr_test() {
            assert_eq!(
                Expr::Number(4.0),
                get_result(r#"let x = 0; loop ( + 2 2) {let x = ( + x 1)} x"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn empty_string_test() {
            assert_eq!(
                Expr::String("".to_owned()),
                get_result(r#""""#.to_owned()).unwrap()
                )
        }
        #[test]
        fn num_string_test() {
            assert_eq!(
                Expr::String("1".to_owned()),
                get_result(r#"( + "" 1)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn string_num_test() {
            assert_eq!(
                Expr::String("1".to_owned()),
                get_result(r#"( + 1 "")"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn round_test() {
            assert_eq!(
                Expr::Number(1.0),
                get_result(r#" (round 1.3) "#.to_owned()).unwrap()
                )
        }
        #[test]
        fn rand_test() {
            assert_eq!(
                Expr::Number(0.0),
                get_result(r#" (* rand 0 ) "#.to_owned()).unwrap()
                )
        }
        #[test]
        fn empty_lambda_test() {
            assert_eq!(
                Expr::Number(1.0),
                get_result(r#" let x = (\ . 1); x "#.to_owned()).unwrap()
                )
        }
        #[test]
        fn floor_test() {
            assert_eq!(
                Expr::Number(1.0),
                get_result(r#" (floor 1.9) "#.to_owned()).unwrap()
                )
        }
        #[test]
        fn ceil_test() {
            assert_eq!(
                Expr::Number(2.0),
                get_result(r#" (ceil 1.3) "#.to_owned()).unwrap()
                )
        }
        #[test]
        fn nil_test() {
            assert_eq!(
                Expr::Nil,
                get_result(r#"nil"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn nil_if_test() {
            assert_eq!(
                Expr::Number(5.0),
                get_result(r#"(ife nil 0 5)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn recursion_test() {
            assert_eq!(
                Expr::Number(0.0),
                get_result(r#"let x = \l. ( ife l (x ( - l 1 )) 0 ); (x 1)"#.to_owned()).unwrap()
                )
        }
        #[test]
        fn length() {
            assert_eq!(
                Expr::Number(4.0),
                get_result(r#"
                          let leng = \l. ( ife (cdr l) (+ 1 (leng (cdr l))) 1); 
                          ( leng ( cons 8 ( cons 2 ( cons 4 ( cons 3 nil) ) ) ) ) 
                           "#.to_owned()).unwrap()
                )
        }
        #[test]
        fn sum_test() {
            assert_eq!(
                Expr::Number(10.0),
                get_result(r#"
                            let sum = \ list . ( 
                            ife (cdr list) 
                                (+ ( car list ) (sum (cdr list))) 
                                (car list ) 
                            ); 
                            ( sum ( cons 3 ( cons 2 ( cons 5 nil ) ) ) )
                           "#.to_owned()).unwrap()
                )
        }
        #[test]
        fn fold() {
            assert_eq!(
                Expr::Number(14.0),
                get_result(r#"
                        let fold = \ c list . ( 
                        ife (cdr list) 
                            (c ( car list ) (fold c (cdr list))) 
                            (car list ) 
                        ); ( fold + ( cons 3 ( cons 2 ( cons 4 ( cons 5 nil ) ) ) ) )
                           "#.to_owned()).unwrap()
                )
        }
        #[test]
        fn fold_lambda() {
            assert_eq!(
                Expr::String("3245".to_owned()),
                get_result(r#"
                        let fold = \ c list . ( 
                        ife (cdr list) 
                            (c ( car list ) (fold c (cdr list))) 
                            (car list ) 
                        ); ( fold ( \x y. ( + x ( + "" y ) ) ) ( cons 3 ( cons 2 ( cons 4 ( cons 5 nil ) ) ) ) )
                           "#.to_owned()).unwrap()
                )
        }
        #[test]
        fn nested_comment_test() {
            assert_eq!(Ok(Expr::Number(8f64)),get_result("(# test (# comment #) #) 8".to_owned()));
        }
        #[test]
        fn broken_comment_test() {
            assert_eq!(Ok(Expr::String("testone two".to_string())),get_result(r#" "test" (#comment#) `+` "one two" (#comment#)"#.to_owned()));
        }
        #[test]
        fn table_creation() {
            use std::collections::HashMap;
            let mut map = HashMap::new();
            map.insert("a".to_owned(),Box::new(Expr::Number(4.0)));
            map.insert("b".to_owned(),Box::new(Expr::Number(8.0)));
            map.insert("c".to_owned(),Box::new(Expr::String("asdf".to_owned())));
            //let map = Cow::Owned(map);
            assert_eq!(
                Expr::Table(map),
                get_result(r#"{a: 4 b: 8 c: "asdf"}"#.to_owned()).unwrap()
            )
        }
        #[test]
        fn table_indexing() {
            assert_eq!(
                Expr::Number(8.0),
                get_result(r#"({a: 4 b: 8 c: "asdf"})["b"]"#.to_owned()).unwrap()
            )
        }
        #[test]
        fn table_indexing_var() {
            assert_eq!(
                Expr::Number(8.0),
                get_result(r#"let table = {a: 4 b: 8 c: "asdf"}; table["b"]"#.to_owned()).unwrap()
            )
        }


    }

