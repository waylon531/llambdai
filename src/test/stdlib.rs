use ast::Expr;
use get_result;
#[test]
fn list2_test() {
    assert_eq!(
        Expr::Number(1.0),
        get_result(r#"
               use "std::list";
               let l = (list2 2 1);
               ( car (cdr l) )
               "#.to_owned()).unwrap()
        )
}
#[test]
fn list4_fold_test() {
    assert_eq!(
        Expr::Number(14.0),
        get_result(r#"
               use "std::list";
               let l = (list4 2 1 7 4);
               ( fold + l )
               "#.to_owned()).unwrap()
        )
}
#[test]
fn list4_map_test() {
    assert_eq!(
        Expr::Number(28.0),
        get_result(r#"
               use "std::list";
               let l = (list4 2 1 7 4);
               let mapped = (map (\x. (* 2 x)) l);
               ( fold + mapped )
               "#.to_owned()).unwrap()
        )
}
#[test]
fn comment_map_test() {
    assert_eq!(
        Expr::Number(28.0),
        get_result(r#"
               use "std::list"; (# import the list library #)
               let l = (list4 2 1 7 4); (#create a list #)
               let mapped = (map (\x. (* 2 x)) l); 
               ( fold  + mapped ) (# Sum the list #)
               "#.to_owned()).unwrap()
        )
}
#[test]
fn num_to_char_quote_test() {
    assert_eq!(
        Expr::String("\"".to_owned()),
        get_result(r#"
               use "std::string"; (# import the string #)
               num_to_char 34
               "#.to_owned()).unwrap()
        )
}
#[test]
fn num_to_char_test() {
    assert_eq!(
        Expr::String("(".to_owned()),
        get_result(r#"
               use "std::string"; (# import the string #)
               num_to_char 40
               "#.to_owned()).unwrap()
        )
}
#[test]
fn multiple_num_to_char_test() {
    assert_eq!(
        Expr::String("test".to_owned()),
        get_result(r#"
               use "std::string"; (# import the string lib #)
               use "std::list"; (# import the list lib #)
               let l = ( list4 116 101 115 116 );
               let lmap = ( map (\x. ( num_to_char x )) l );
               fold + lmap
               "#.to_owned()).unwrap()
        )
}
#[test]
fn list_to_string_test() {
    assert_eq!(
        Expr::String("test".to_owned()),
        get_result(r#"
               use "std::string"; (# import the string lib #)
               use "std::list"; (# import the list lib #)
               let l = ( list4 116 101 115 116 );
               list_to_string l
               "#.to_owned()).unwrap()
        )
}
#[test]
fn rev_list_to_string_test() {
    assert_eq!(
        Expr::String("tset".to_owned()),
        get_result(r#"
               use "std::string"; (# import the string lib #)
               use "std::list"; (# import the list lib #)
               let l = ( list4 116 101 115 116 );
               rev_list_to_string l
               "#.to_owned()).unwrap()
        )
}
#[test]
fn eq_test() {
    assert_eq!(
        Expr::Number(1.0),
        get_result(r#"
               use "std::core"; (# import the string #)
               eq 42 42"#.to_owned()).unwrap()
        )
}
#[test]
fn not_test() {
    assert_eq!(
        Expr::Number(0.0),
        get_result(r#"
               use "std::core"; (# import the string #)
               not 1"#.to_owned()).unwrap()
        )
}
#[test]
fn zip_test() {
    assert_eq!(
        Expr::List(Box::new(Expr::List(Box::new(Expr::Number(1.0)),Box::new(Expr::List(Box::new(Expr::Number(2.0)),Box::new(Expr::Nil))))),
        Box::new(Expr::List(Box::new(Expr::List(Box::new(Expr::Number(1.0)),Box::new(Expr::List(Box::new(Expr::Number(2.0)),Box::new(Expr::Nil))))), Box::new(Expr::Nil)))),
        get_result(r#"
               use "std::list"; (# import the string #)
               zip (list2 1 1 ) (list2 2 2)"#.to_owned()).unwrap()
        )
}
#[test]
fn list_compare_test() {
    assert_eq!(
        Expr::Number(1.0),
        get_result(r#"
               use "std::list"; (# import the string #)
               cmp (list2 1 2) (list2 1 2)"#.to_owned()).unwrap()
        )
}
#[test]
fn list_not_compare_test() {
    assert_eq!(
        Expr::Number(0.0),
        get_result(r#"
               use "std::list"; (# import the string #)
               cmp (list2 1 2) (list2 2 1)"#.to_owned()).unwrap()
        )
}
#[test]
fn foldr_test() {
    assert_eq!(
        Expr::Number(48.0),
        get_result(r#"
               use "std::list"; (# import the string #)
               foldr * 2 (list4 1 2 3 4)"#.to_owned()).unwrap()
        )
}
#[test]
fn foldl_test() {
    assert_eq!(
        Expr::Number(48.0),
        get_result(r#"
               use "std::list"; (# import the string #)
               foldl * 2 (list4 1 2 3 4)"#.to_owned()).unwrap()
        )
}
#[test]
fn reverse_test() {
    assert_eq!(
        Expr::List(Box::new(Expr::Number(2.0)),Box::new(Expr::List(Box::new(Expr::Number(1.0)),Box::new(Expr::Nil)))),
        get_result(r#"
               use "std::list"; (# import the string #)
            reverse (list2 1 2 )"#.to_owned()).unwrap()
        )
}
#[test]
fn reverse_sum_test() {
    assert_eq!(
        Expr::Number(10.0),
        get_result(r#"
            use "std::list"; (# import the string #)
            let rev = (reverse (list4 1 2 3 4 )) ;
            fold + rev"#.to_owned()).unwrap()
        )
}
